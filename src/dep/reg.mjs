
import semver from 'semver'

export default class DependencyRegistry {
  constructor(name, cfg, mg) {
    this.name = name;
    this.cfg = cfg;
    this.mg = mg;

    this.project_callback = cfg.project;
    this.versions_callback = cfg.versions;
    this.parser_callback = cfg.parser;
    this.fetch_callback = cfg.fetch;

    this.versions = {};
    this.packages = {};
  }

  async read_project(project_path) {
    return await this.project_callback(project_path);
  }

  async parse(pkg_info) {
    const pkg_key = pkg_info.type ? `${pkg_info.type}/${pkg_info.name}` : pkg_info.name;


    if (semver.valid(pkg_info.version_expression)) {
      pkg_info.version = pkg_info.version_expression;
    } else if (semver.validRange(pkg_info.version_expression) !== null) {
      if (!this.versions[pkg_key]) {
        this.versions[pkg_key] = await this.mg.regs[pkg_info.reg].versions_callback(pkg_info)
      }
      pkg_info.version = semver.maxSatisfying(this.versions[pkg_key], pkg_info.version_expression);
    }

    const pkg_info_key = `${pkg_key}@${pkg_info.version}`;
    if (!this.packages[pkg_info_key]) {
      pkg_info = this.packages[pkg_info_key] = await this.mg.regs[pkg_info.reg].parser_callback(pkg_info);
    } else {
      pkg_info = this.packages[pkg_info_key];
    }

    if (!pkg_info.deps) pkg_info.deps = [];

    for (let d = 0; d < pkg_info.deps.length; d++) {
      const sub_pkg_key = pkg_info.deps[d].type ?
        `${pkg_info.deps[d].type}/${pkg_info.deps[d].name}` :
        pkg_info.deps[d].name;

      if (semver.valid(pkg_info.deps[d].version_expression)) {
        pkg_info.deps[d].version = pkg_info.deps[d].version_expression;
      } else if (semver.validRange(pkg_info.deps[d].version_expression) !== null) {
        if (!this.versions[sub_pkg_key]) {
          this.versions[sub_pkg_key] = await this.mg.regs[pkg_info.deps[d].reg].versions_callback(pkg_info.deps[d]);
        }
        pkg_info.deps[d].version = semver.maxSatisfying(this.versions[sub_pkg_key], pkg_info.deps[d].version_expression);
      }

      const sub_pkg_info_key = `${sub_pkg_key}@${pkg_info.deps[d].version}`;

      if (!this.packages[sub_pkg_info_key]) {
        pkg_info.deps[d] = await this.mg.regs[pkg_info.deps[d].reg].parse(pkg_info.deps[d]);
      } else {
        pkg_info.deps[d] = this.packages[sub_pkg_info_key];
      }
    }

    return pkg_info;
  }

  async fetch(pkg_info) {
    return await this.fetch_callback(pkg_info);
  }

}
