
import fs from 'fs'
import path from 'path'

import * as tar from 'tar'
import { pipeline } from 'stream';

import Registry from './reg.mjs'
import Cache from './cache.mjs'

export default class DependencyManager {
  constructor(project_path, regs) {
    this.project_path = project_path;
    this.regs = {};
    this.caches = {};
    for (let reg in regs) {
      this.regs[reg] = new Registry(reg, regs[reg], this);
      this.caches[reg] = new Cache(reg, regs[reg]);
    }

    this.deps = [];
  }

  add_deps(pkg) {
    let already_added = false;
    for (let edep of this.deps) {
      if (edep.name == pkg.name && edep.type == pkg.type && edep.version == pkg.version) {
        already_added = true;
        break;
      }
    }


    if (!already_added) {
      this.deps.push(pkg);
    }

    if (!pkg.deps) pkg.deps = [];


    for (let dep of pkg.deps) {
      let dependency_conflict = false;
      for (let edep of this.deps) {
        if (edep.name == dep.name && edep.type == dep.type) {
          dependency_conflict = true;
          break;
        }
      }

      if (dependency_conflict) dep.dependent = pkg;
      this.add_deps(dep);
    }
    delete pkg.deps;
  }

  async parse(reg_name = "nest") {
    const pkg_deps = await this.regs[reg_name].read_project(this.project_path);

    for (let dep of pkg_deps) {
      const dep_info = await this.regs[dep.reg].parse(dep);
      this.add_deps(dep_info);
    }

  }

  async install_dependent(reg_name = "nest", dep) {
    let cached = await this.caches[reg_name].retrieve(dep);
    if (!cached) {
      const response = await this.regs[reg_name].fetch(dep);
      const archive_name = await this.caches[reg_name].archive_name(dep);
      cached = await this.caches[reg_name].store(response, archive_name, dep.type);
    }
    const node_modules_path = path.join(dep.dependent.install_path, "node_modules");
    await this.caches[reg_name].extract(dep, cached, node_modules_path);
  }

  async install(reg_name = "nest") {


    for (let dep of this.deps) {
      if (dep.reg == reg_name && !dep.dependent) {
        let cached = await this.caches[reg_name].retrieve(dep);
        if (!cached) {
          const response = await this.regs[reg_name].fetch(dep);
          const archive_name = await this.caches[reg_name].archive_name(dep);
          cached = await this.caches[reg_name].store(response, archive_name, dep.type);
//          await this.regs[reg_name].extract(dep, response);
        }
        await this.caches[reg_name].extract(dep, cached);
      }


    }

    for (let dep of this.deps) {
      if (dep.reg == reg_name && dep.dependent) {
        let sub_dep = dep;
        while (sub_dep.dependent && !sub_dep.dependent.install_path) {
          const old_dep = sub_dep;
          if (sub_dep.dependent) {
            sub_dep = sub_dep.dependent;
            sub_dep.dependency = old_dep;
          }
        }

        while (sub_dep) {
          await this.install_dependent(reg_name, sub_dep);
          sub_dep = sub_dep.dependency;
        }


        let cached = await this.caches[reg_name].retrieve(dep);
        if (!cached) {
          const response = await this.regs[reg_name].fetch(dep);
          const archive_name = await this.caches[reg_name].archive_name(dep);
          cached = await this.caches[reg_name].store(response, archive_name, dep.type);
        }
        const node_modules_path = path.join(dep.dependent.install_path, "node_modules");
        await this.caches[reg_name].extract(dep, cached, node_modules_path);
      }
    }

  }
}
