
import fs from 'fs'
import os from 'os'
import path from 'path'

import { createWriteStream } from 'fs';
import { pipeline } from 'stream';


export default class DependencyCache {
  constructor(reg_name, cfg) {
    this.cache_path = path.join(os.homedir(), `.zunzun/cache/${reg_name}`);
    fs.mkdirSync(this.cache_path, { recursive: true });

    this.filename_callback = cfg.filename;
    this.extract_callback = cfg.extract;
  }

  async archive_name(pkg_info) {
    return await this.filename_callback(pkg_info);
  }

  async retrieve(pkg_info) {
    const archive_name = await this.archive_name(pkg_info);
    console.log("archive_name", archive_name);
    const archive_path = path.join(this.cache_path, archive_name);

    console.log("archive_path", archive_path);
    if (fs.existsSync(archive_path)) {
      return archive_path;
    } else {
      return undefined;
    }
  }

  async store(response, archive_name, pkg_type) {
    if (!response.ok) {
      throw new Error(`Failed to fetch: ${response.statusText}`);
    }

    const destination_dir_path = pkg_type ? path.join(this.cache_path, pkg_type) : this.cache_path;
    fs.mkdirSync(destination_dir_path, { recursive: true });
    archive_name = archive_name.split('/').pop();
    const destination_path = path.join(destination_dir_path, archive_name);
    console.log("destination_path", destination_path);
/*
    const totalSize = response.headers.get('Content-Length');
    let downloadedSize = 0;
    // The stream to handle the response body
    const reader = response.body.getReader();
    const writer = createWriteStream(destination_path);

    // Function to handle the data as it's downloaded
    const pump = async () => {
      const { done, value } = await reader.read();
      if (done) {
        console.log('Download complete.');
        return;
      }

      // Write the chunk to the destination file
      writer.write(value);

      // Update the downloaded size
      downloadedSize += value.length;

      // Calculate and log the percentage downloaded
      const percentage = ((downloadedSize / totalSize) * 100).toFixed(2);
      console.log(`Download progress: ${percentage}%`);

      // Keep reading the next chunk
      await pump();
    };

    await pump();*/

    await new Promise((resolve) => {
      pipeline(
        response.body,
        createWriteStream(destination_path),
        (err) => {
          if (err) {
            console.error('Caching failed:', err);
          } else {
            console.log(`Done caching ${destination_path}`);
          }
          resolve();
        }
      );
    });

    return destination_path;
  }

  async extract(pkg_info, archive_path, node_modules_path) {
    return await this.extract_callback(pkg_info, archive_path, node_modules_path);
  }
}
