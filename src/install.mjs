
import fs from 'fs'
import path from 'path'

import * as tar from 'tar'
import { pipeline } from 'stream';

import AES from 'zunzun/flypto/aes.mjs'
import CLIUtil from 'zunzun/flyutil/cli.mjs'
import Exec from 'zunzun/flyutil/exec.mjs'
import FSUtil from 'zunzun/flyutil/fs.mjs'
import RSA from 'zunzun/flypto/rsa.mjs'

//import DependencyManager from './dep/mg.mjs'

const ignore_dirs = [".git"];

export default class Install {
  static async download_and_extract(type, name, version, remote_path, install_path, twig_dir, zunzun_cfg, installed_twigs, auto_yes) {
    try {
      console.log(`Installing ${type} twig "${name}@${version}" to "${install_path}"`);

      const result_dir = path.resolve(install_path, name);
      if (fs.existsSync(result_dir)) {
        console.log(`Directory "${result_dir}" already exists! Proceeding with this installation will remove it.`);
        if (!auto_yes) {
          let rl = CLIUtil.InputReader();
          let answer = await rl.questionSync(`Are you sure you want to continue? (y/n): `);
          while (answer !== "y") {
            if (answer === "n") {
              rl.close();
              return;
            }
            answer = await rl.questionSync(`Are you sure you want to continue? (y/n): `);
          }
          rl.close();
          try { fs.rmSync(result_dir, { recursive: true }) } catch (e) { }
        } else {
          console.log("Removing since automatic accepting is enabled!");
          try { fs.rmSync(result_dir, { recursive: true }) } catch (e) { }
        }
      }

      const tmp_dir = type == "lib" || type == "zrv" ? ".zun/tmp" : zunzun_cfg.zunzun_path+"/.tmp";

      Exec.command(`mkdir -p ${tmp_dir}/dl`);
      Exec.command(`curl ${zunzun_cfg.repo_url}${remote_path} > ${tmp_dir}/dl/${type}-${name}-${version}.tar.gz`);
      Exec.command(`mkdir -p ${install_path}`);
      Exec.command(`tar -zxvf ${tmp_dir}/dl/${type}-${name}-${version}.tar.gz -C ${install_path}`);

      const pack_json = path.resolve(result_dir, 'package.json');
      if (fs.existsSync(pack_json)) Exec.command(`npm install`, result_dir, true);


      installed_twigs.push(`${type}/${name}`);

      const twig_json = JSON.parse(fs.readFileSync(path.resolve(result_dir, "twig.json"), "utf8"));
      if (twig_json.lib) {
        for (let lib in twig_json.lib) {
          if (!installed_twigs.includes(`lib/${lib}`)) {
            await Install.one("lib", lib, twig_dir, zunzun_cfg, installed_twigs, auto_yes);
          }
        }
      }
      if (twig_json.srv) {
        for (let srv in twig_json.srv) {
          if (!installed_twigs.includes(`srv/${srv}`)) {
            await Install.one("srv", srv, twig_dir, zunzun_cfg, installed_twigs, auto_yes);
          }
        }
      }

      if (type === "lib") {
        const nmz_path = path.resolve(install_path, "../../node_modules/zunzun");
        Exec.command(`mkdir -p ${nmz_path}`);
        Install.link(twig_dir, name, result_dir);
      }

      Exec.command(`rm -rf ${tmp_dir}`);
    } catch (e) {
      console.error(e.stack);
    }
  }

  static link(twig_dir, dep_name, dep_path) {
    const npm_path = path.resolve(twig_dir, "node_modules", "zunzun", dep_name);
    try { fs.rmSync(npm_path, { recursive: true }) } catch (e) { }
    fs.symlinkSync(dep_path, npm_path);
  }


  static async one(type, name, twig_dir, zunzun_cfg, installed_twigs, auto_yes) {
    try {

      console.log("Retrieving public key...");
      const flyauth_rsa = new RSA(
        await fetch(`${zunzun_cfg.repo_url}/flyauth/public-key`).then(res => res.text())
      );
      console.log("Done!");

      console.log(`Retrieving latest verion of ${type} twig "${name}"...`);
      const secret_aes = new AES();
      const enc_data = flyauth_rsa.encrypt({
        data: {
          twigs: [
            [type, name]
          ],
          secret: secret_aes.key
        }
      });
      const latest_versions = JSON.parse(secret_aes.decrypt(
        await fetch(`${zunzun_cfg.repo_url}/twigs-latest_version?data=${enc_data}`).then(res => res.text())
      ));

      if (latest_versions.length > 0) {
        const version = latest_versions[0][1];
        console.log(`Found ${type} twig "${name}@${version}"!`);

        const remote_path = `/nest/${type}/${name}/${type}-${name}@${version}.tar.gz`;


        if (type === "cli" || type === "tpl") {
          const install_path = path.resolve(zunzun_cfg.zunzun_path, type);
          const g_twig_dir = path.resolve(zunzun_cfg.zunzun_path, type, name);
          await Install.download_and_extract(type, name, version, remote_path, install_path, g_twig_dir, zunzun_cfg, installed_twigs, auto_yes);
        } else if (type === "lib" || type === "srv") {
          const install_path = path.resolve(twig_dir, "twigs", type);
          await Install.download_and_extract(type, name, version, remote_path, install_path, twig_dir, zunzun_cfg, installed_twigs, auto_yes);
        } else {
          console.error(new Error(`Invalid twig type "${type}"!`));
        }

      } else {
        console.log(`Error: ${type} twig "${name}" not found!`);
        return;
      }

    } catch (e) {
      console.error(e.stack);
    }

  }


  static async by_twig_cfg(twig_dir, twig_json, zunzun_cfg, auto_yes) {
    try {
      /*
      const dep_mg = new DependencyManager(twig_dir, {
        nest: {
          project: (project_path) => {

            const deps = [];

            const pkg_twig_path = path.join(project_path, "twig.json");
            const pkg_twig = JSON.parse(
              fs.readFileSync(pkg_twig_path, 'utf8')
            );

            for (let dep_type of ["srv", "lib"]) {

              for (let dep_name in pkg_twig[dep_type]) {
                deps.push({
                  reg: 'nest',
                  type: dep_type,
                  name: dep_name,
                  version_expression: pkg_twig[dep_type][dep_name]
                });
              }
            }

            const pkg_json_path = path.join(project_path, "package.json");
            if (fs.existsSync(pkg_json_path)) {
              const pkg = JSON.parse(
                fs.readFileSync(pkg_json_path, 'utf8')
              );


              for (let dep_name in pkg.dependencies) {
                const version_expression = pkg.dependencies[dep_name]
                deps.push({
                  reg: 'npm',
                  name: dep_name,
                  version_expression
                });
              }

              for (let dep_name in pkg.devDependencies) {
                const version_expression = pkg.devDependencies[dep_name]
                deps.push({
                  reg: 'npm',
                  name: dep_name,
                  version_expression
                });
              }
            }

            return deps;
          },
          versions: async (pkg_info) => {
            console.log(pkg_info);
            const ver_url = `${zunzun_cfg.repo_url}/nest/${pkg_info.type}/${pkg_info.name}/info`
            console.log("NEST FETCH", ver_url);
            const reg_pkg_info = await (await fetch(ver_url)).json();
            if (!pkg_info.name) throw new Error("Invalid request!");
            return reg_pkg_info.versions;
          },
          parser: async (pkg_info) => {
            console.log("PARSE", pkg_info);
            const ver_url = `${zunzun_cfg.repo_url}/nest/${pkg_info.type}/${pkg_info.name}/twig@${pkg_info.version}.json`;
            console.log("FETCH", ver_url);
            const reg_pkg_info = await (await fetch(ver_url)).json();;
            if (!pkg_info.name) throw new Error("Invalid request!");
            pkg_info.deps = [];

            
            for (let dep_type of ["srv", "lib"]) {
              for (let dep in reg_pkg_info[dep_type]) {
                pkg_info.deps.push({
                  reg: 'nest',
                  type: dep_type,
                  name: dep,
                  version_expression: reg_pkg_info[dep_type][dep]
                });
              }
            }

            const package_json_url = `${zunzun_cfg.repo_url}/nest/${pkg_info.type}/${pkg_info.name}/package@${pkg_info.version}.json`;
            console.log("TWIG PKG JSON URL", package_json_url);
            const package_json = await (await fetch(package_json_url)).json();
            console.log("PKG", package_json);
            
            for (let dep in package_json.dependencies) {
              console.log("PUSH DEP", dep);
              pkg_info.deps.push({
                reg: 'npm',
                name: dep,
                version_expression: package_json.dependencies[dep]
              });
            }

            for (let dep in package_json.devDependencies) {
              console.log("PUSH DEP", dep);
              pkg_info.deps.push({
                reg: 'npm',
                name: dep,
                version_expression: package_json.devDependencies[dep]
              });
            }

            console.log("DONE PARSING", pkg_info);

            return pkg_info;
          },
          filename: async (pkg_info) => {
            return `${pkg_info.name}@${pkg_info.version}.tgz`;
          },
          fetch: async (pkg_info) => {
            const pkg_url = `${zunzun_cfg.repo_url}/nest/${pkg_info.type}/${pkg_info.name}/${pkg_info.type}-${pkg_info.name}@${pkg_info.version}.tar.gz`;
            console.log("FETCH", pkg_url);
            return await fetch(pkg_url);
          },
          extract: async (pkg_info, archive_path, node_modules_path) => {
            const outputDir = path.join(twig_dir, "twigs", pkg_info.type, pkg_info.name);
            fs.mkdirSync(outputDir, { recursive: true });

            pkg_info.install_path = outputDir;
            console.log("EXTRACT", pkg_info);

            await tar.extract({
              file: archive_path, // Path to the archive
              cwd: outputDir, // Extract to the destination directory
              strip: 1, // Preserve full directory structure (set to 1 to remove top-level folder)
            });

            const npm_path = path.resolve(twig_dir, "node_modules", "zunzun", pkg_info.name);
            try { fs.rmSync(npm_path, { recursive: true }) } catch (e) { }
            fs.symlinkSync(outputDir, npm_path);
          }
        },
        npm: {
          project: (project_path) => {

            const deps = [];

            const pkg_json_path = path.join(project_path, "package.json");
            const pkg = JSON.parse(
              fs.readFileSync(pkg_json_path, 'utf8')
            );


            for (let dep_name in pkg.dependencies) {
              const version_expression = pkg.dependencies[dep_name]
              deps.push({
                reg: 'npm',
                name: dep_name,
                version_expression
              });
            }

            for (let dep_name in pkg.devDependencies) {
              const version_expression = pkg.devDependencies[dep_name]
              deps.push({
                reg: 'npm',
                name: dep_name,
                version_expression
              });
            }
            return deps;
          },
          versions: async (pkg_info) => {
            console.log("Looking up available versions for package", pkg_info.name);
            const ver_url = `https://registry.npmjs.org/${pkg_info.name}`
            const reg_pkg_info = await (await fetch(ver_url)).json();
            if (!reg_pkg_info.name) {
              console.log("RESP JSON", reg_pkg_info);
              throw new Error("Invalid request!");
            }
            return Object.keys(reg_pkg_info.versions);
          },
          parser: async (pkg_info) => {
            console.log("Looking up dependencies for package", `${pkg_info.name}@${pkg_info.version}`);
            const url = `https://registry.npmjs.org/${pkg_info.name}/${pkg_info.version}`;
            const reg_pkg_info = await (await fetch(url)).json();
            if (!pkg_info.name) throw new Error("Invalid request!");
            pkg_info.deps = [];
            for (let dep in reg_pkg_info.dependencies) {
              pkg_info.deps.push({
                reg: 'npm',
                name: dep,
                version_expression: reg_pkg_info.dependencies[dep]
              });
            }
            for (let dep in reg_pkg_info.devDependencies) {
              pkg_info.deps.push({
                reg: 'npm',
                name: dep,
                version_expression: reg_pkg_info.devDependencies[dep]
              });
            }
            return pkg_info;
          },
          filename: async (pkg_info) => {
            return `${pkg_info.name}@${pkg_info.version}.tgz`;
          },
          fetch: async (pkg_info) => {
            console.log("Downloading package", `${pkg_info.name}@${pkg_info.version}`);
            const archive_name_prefix = pkg_info.name.split('/').pop();
            const pkg_url = `https://registry.npmjs.org/${pkg_info.name}/-/${archive_name_prefix}-${pkg_info.version}.tgz`;
           
            return await fetch(pkg_url);
          },
          extract: async (pkg_info, archive_path, node_modules_path) => {
            console.log("Extracting package", `${pkg_info.name}@${pkg_info.version}`);
            const outputDir = node_modules_path ?
              path.join(node_modules_path, pkg_info.name) :
              path.join(twig_dir, "node_modules", pkg_info.name);
            fs.mkdirSync(outputDir, { recursive: true });

            pkg_info.install_path = outputDir;

            await tar.extract({
              file: archive_path, // Path to the archive
              cwd: outputDir, // Extract to the destination directory
              strip: 1, // Preserve full directory structure (set to 1 to remove top-level folder)
            });
          }
        }
      });

//      await dep_mg.parse('nest');
      await dep_mg.parse('npm');

//      await dep_mg.install('nest');
      await dep_mg.install('npm');*/

      const install_types = ["lib", "srv"];
      const dls = [];
      for (let itype of install_types) {
        if (twig_json[itype]) {
          const versions = twig_json[itype];
          for (let twig in versions) {

            dls.push({
              type: itype,
              name: twig,
              version: versions[twig]
            });


          }
        }
      }

      const get_versions = [];
      for (let dl of dls) {
        if (dl.version === "*") {
          get_versions.push([dl.type, dl.name]);
        }
      }


      const flyauth_rsa = new RSA(
        await fetch(`${zunzun_cfg.repo_url}/flyauth/public-key`).then(res => res.text())
      );

      console.log("GET LATEST VERSIONS");
      const secret_aes = new AES();
      const enc_data = flyauth_rsa.encrypt({
        data: {
          twigs: get_versions,
          secret: secret_aes.key
        }
      })
      const latest_versions = JSON.parse(secret_aes.decrypt(
        await fetch(`${zunzun_cfg.repo_url}/twigs-latest_version?data=${enc_data}`).then(res => res.text())
      ));
      console.log(latest_versions);

      for (let lver of latest_versions) {
        for (let d = 0; d < dls.length; d++) {
          if (dls[d].name === lver[0]) {
            dls[d].version = lver[1];
          }
        }

      }

      console.log("dls", dls);

      Exec.command(`mkdir -p .zun/tmp/dl`);


      const installed_twigs = [];

      for (let dl of dls) {
        if (dl.version !== "*") {
          const remote_path = `/nest/${dl.type}/${dl.name}/${dl.type}-${dl.name}@${dl.version}.tar.gz`;
          const install_path = path.resolve(twig_dir, "twigs", dl.type);

          await Install.download_and_extract(dl.type, dl.name, dl.version, remote_path, install_path, twig_dir, zunzun_cfg, installed_twigs, auto_yes);


        } else {
          console.warn(`Twig "${dl.name}" not found!`);
        }
      }

    } catch (e) {
      console.error(e.stack);
      return undefined;
    }
  }
  
  static local(twig_dir, twig_json, zunzun_cfg) {
    if (twig_json.lib) {
      for (let lib in twig_json.lib) {
        console.log(zunzun_cfg.local_twigs, "lib", lib);
        const local_path = path.resolve(zunzun_cfg.local_twigs, "lib", lib);
        const install_path = path.resolve(twig_dir, "twigs/lib", lib);
        try { fs.rmSync(install_path, { recursive: true }) } catch (e) { }
        fs.symlinkSync(local_path, install_path);
      }
    }
    if (twig_json.srv) {
      for (let srv in twig_json.srv) {
        console.log(zunzun_cfg.local_twigs, "srv", srv);
        const local_path = path.resolve(zunzun_cfg.local_twigs, "srv", srv);
        const install_path = path.resolve(twig_dir, "twigs/srv", srv);
        try { fs.rmSync(install_path, { recursive: true }) } catch (e) { }
        fs.symlinkSync(local_path, install_path);
      }
    }
  }

  static sync_local_twig(twig_type, twig_name, twig_dir, zunzun_cfg) {
    const local_path = path.resolve(zunzun_cfg.local_twigs, twig_type, twig_name);
    const install_path = path.resolve(twig_dir, "twigs", twig_type, twig_name);
    try { fs.rmSync(install_path, { recursive: true }) } catch (e) { }
    FSUtil.sync_dirs(local_path, install_path, ignore_dirs);

    if (twig_type === "lib") Install.link(twig_dir, twig_name, install_path);
  }

  static sync_local(twig_dir, twig_json, zunzun_cfg) {
    const deps = [];
    Install.get_dependencies(deps, twig_dir, twig_json, zunzun_cfg);

    for (let dep of deps) {
      if (zunzun_cfg.local_selective && !zunzun_cfg.local_selective_disabled) {
        if (
          zunzun_cfg.local_selective[dep.type] &&
          zunzun_cfg.local_selective[dep.type].includes(dep.name)
        ) {
          this.sync_local_twig(dep.type, dep.name, twig_dir, zunzun_cfg);
        }
      } else {
        this.sync_local_twig(dep.type, dep.name, twig_dir, zunzun_cfg);
      }
    }
  }

  static get_dependencies(deps, twig_dir, twig_json, zunzun_cfg) {
    for (let type of ['lib', 'srv']) {
      for (let lib in twig_json[type]) {
        const dep = {
          type: type,
          name: lib,
          version: twig_json[type][lib]
        }

        const local_path = path.resolve(zunzun_cfg.local_twigs, type, lib);
        const lib_twig_json_path = path.resolve(local_path, 'twig.json');
        const lib_twig_json = JSON.parse(fs.readFileSync(lib_twig_json_path, 'utf8'));

        let duplicate = false;
        for (let edep of deps) {
          if (
            dep.type == edep.type &&
            dep.name == edep.name
          ) {
            duplicate = true;

            // TODO compare versions
          }
        }
        if (!duplicate) deps.push(dep);

        Install.get_dependencies(deps, twig_dir, lib_twig_json, zunzun_cfg);
      }
    }

    return deps;
  }

  static sync_local_watch(twig_dir, twig_json, zunzun_cfg) {
    const deps = [];
    Install.get_dependencies(deps, twig_dir, twig_json, zunzun_cfg);

    for (let dep of deps) {
      console.log(dep.type, dep.name);

      const local_path = path.resolve(zunzun_cfg.local_twigs, dep.type, dep.name);
      const install_path = path.resolve(twig_dir, "twigs", dep.type, dep.name);
      try { fs.rmSync(install_path, { recursive: true }) } catch (e) { }
      FSUtil.sync_dirs_watch(local_path, install_path, ignore_dirs);

      if (dep.type === "lib") Install.link(twig_dir, dep.name, install_path);
    }
  }

  static async by_command(twig_dir, args, zunzun_cfg) {
    try {
      const twig_json_path = path.resolve(twig_dir, 'twig.json');
      const twig_json = fs.existsSync(twig_json_path) ?
        JSON.parse(fs.readFileSync(twig_json_path, 'utf8')) : undefined;

      if (
        args[0] === "lib" ||
        args[0] === "srv" ||
        args[0] === "--local" ||
        args[0] === "--sync-local" ||
        args[0] === "--sync-local-watch" ||
        (
          args.length == 0 ||
          (args.length == 1 && args[0] == '--yes')
        )
      ) {
        if (!twig_json) {
          throw new Error(`Package configuration file '${twig_json_path}' does not exist!`);
        }
        Exec.command(`mkdir -p twigs/lib`);
        Exec.command(`mkdir -p twigs/srv`);
        Exec.command(`mkdir -p node_modules/zunzun`);
      }


      if (args.length > 0 && !(args.length == 1 && args[0] == '--yes')) {
        if (args[0] === "--local") {
          Install.local(twig_dir, twig_json, zunzun_cfg);
        } else if (args[0] === "--sync-local") {
          Install.sync_local(twig_dir, twig_json, zunzun_cfg);
        } else if (args[0] === "--sync-local-watch") {
          Install.sync_local_watch(twig_dir, twig_json, zunzun_cfg);
        } else if (args.length > 1 && (
            args[0] === "cli" ||
            args[0] === "lib" ||
            args[0] === "srv" ||
            args[0] === "tpl"
        )) {
          const installed_twigs = [];
          await Install.one(args[0], args[1], twig_dir, zunzun_cfg, installed_twigs, args.includes("--yes"));
        }
      } else {
        await Install.by_twig_cfg(twig_dir, twig_json, zunzun_cfg, args.includes("--yes"));
      }
    } catch (e) {
      console.error(e.stack);
      return undefined;
    }
  }
}
