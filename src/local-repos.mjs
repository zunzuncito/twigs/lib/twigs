
import fs from 'fs'
import path from 'path'

import Exec from 'zunzun/flyutil/exec.mjs'

const pkg_types = [
  'lib', 'srv', 'cli', 'tpl'
]

export default class LocalRepos {

  static async clone(args, gcfg) {
    console.log(gcfg);
    for (let group of gcfg.local_twigs_repos) {
      group.shh


      for (let type of pkg_types) {
        if (group[type]) {
          for (let pkg of group[type]) {
            const cwd = `${gcfg.local_twigs}/${type}`;
            if (!fs.existsSync(cwd)) fs.mkdirSync(cwd);
            const cmd = `git clone ${group.ssh}/${type}/${pkg}`; 

            const expected_path = path.resolve(cwd, pkg);
            if (!fs.existsSync(expected_path)) {
              Exec.command(cmd, cwd, true);
            } else {
              console.log(`Local package "${type}/${pkg}" repository already present`);
            }
          }
        }
      }

    }

  }


}
