
import fs from 'fs'
import path from 'path'

import semver from 'semver'

import AES from 'zunzun/flypto/aes.mjs'
import Exec from 'zunzun/flyutil/exec.mjs'
import RSA from 'zunzun/flypto/rsa.mjs'

const valid_twig_types = [ 'cli', 'lib', 'srv', 'tpl' ];

export default class Publish {
  static validate(twig_json) {
    if (
      typeof twig_json.type !== 'string' ||
      twig_json.type.length !== 3 ||
      !valid_twig_types.includes(twig_json.type)
    ) {
      return new Error("Invalid `type` property defined in `twig.json` file!");
    }

    if (
      typeof twig_json.name !== 'string'
    ) {
      return new Error("Invalid `name` property defined in `twig.json` file!");
    }

    if (
      typeof twig_json.version !== 'string' ||
      !semver.valid(twig_json.version)
    ) {
      return new Error("Invalid `version` property defined in `twig.json` file!");
    }

    if (
      typeof twig_json.repository !== 'string' ||
      !twig_json.repository.startsWith("https://")
    ) {
      return new Error("Invalid `repository` property defined in `twig.json` file!");
    }

    if (
      typeof twig_json.branch !== 'string'
    ) {
      return new Error("Invalid `branch` property defined in `twig.json` file!");
    }

    return true;
  }

  static async one(twig_dir, args, zunzun_cfg) {
    try {
      const twig_json_path = path.resolve(twig_dir, 'twig.json');
      const twig_json = JSON.parse(fs.readFileSync(twig_json_path, 'utf8'));

      const valid = Publish.validate(twig_json);
      if (!(valid instanceof Error)) {
        Exec.command(`mkdir -p .zun/tmp`);
        Exec.command(`git fetch`);
        const main_branch = Exec.command(`git branch --show-current`);
        Exec.command(`git archive --format=tar.gz -o .zun/tmp/${twig_json.name}.tar.gz --prefix=${twig_json.name}/ ${main_branch}`, twig_dir, true);
        const sha256sum = Exec.command(`sha256sum .zun/tmp/${twig_json.name}.tar.gz`);

        const publish_data = {
          type: twig_json.type,
          name: twig_json.name,
          version: twig_json.version,
          license: twig_json.license,
          repository: twig_json.repository,
          branch: twig_json.branch,
          sha256sum: sha256sum.slice(0, 64)
        }

        console.log("publish_data", publish_data);
        const signature = Exec.command(`echo "${JSON.stringify(publish_data)}" | ssh-keygen -Y sign -n file -f ${zunzun_cfg.ed25519_key} | tee .zun/tmp/publish-${twig_json.version}.sig`);


        console.log("zunzun_cfg.repo_url", zunzun_cfg.repo_url);
        const public_key_rsa = await fetch(`${zunzun_cfg.repo_url}/flyauth/public-key`).then(res => res.text());


        const flyauth_rsa = new RSA(public_key_rsa);

        const secret_aes = new AES();
        const enc_data = flyauth_rsa.encrypt({
          data: {
            twigs: [
              [twig_json.type, twig_json.name]
            ],
            secret: secret_aes.key
          }
        })
        const latest_versions = JSON.parse(secret_aes.decrypt(
          await fetch(`${zunzun_cfg.repo_url}/twigs-latest_version?data=${enc_data}`).then(res => res.text())
        ));


        if (latest_versions.length == 0 || semver.gt(twig_json.version, latest_versions[0][1])) {
          const req_data = {
            signature: signature,
            data: publish_data,
            username: zunzun_cfg.nest_username,
            key_label: zunzun_cfg.nest_key_label
          };
          console.log("REQ DATA", req_data);
          const resp_val = await fetch(`${zunzun_cfg.repo_url}/publish-twig`, {
            method: "PUT",
            body: "data="+flyauth_rsa.encrypt(req_data)
          }).then((res) => {
            if (res.ok) {
              return res.text()
            } else {
              return res.statusText;
            }
          });

          if (resp_val === "Unauthorized") {
            console.error(new Error(
              `Error: Unauthorized access! Check you local configuration properties.`
            ));
          } else if (resp_val === "Unprocessable Entity") {
            console.error(new Error(
              `Error: Unprocessable Entity.`
            ));
          } else {
            console.log(`Twig '${publish_data.type} > ${publish_data.name}@${publish_data.version}' has been published successfuly!`);
          }
        } else {
          console.error(new Error(
            `the version you want to publish "${twig_json.version}" is not greater then the current latest version "${latest_versions[0][1]}"!`
          ));
        }

      } else {
        console.error(valid);
      }

      Exec.command(`rm -rf .zun`);
    } catch (e) {
      console.error(e.stack);
      return undefined;
    }
  }
}
