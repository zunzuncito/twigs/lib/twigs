
import fs from 'fs'
import path from 'path'

import YAML from 'yaml'

import CLIUtil from 'zunzun/flyutil/cli.mjs'
import FSUtil from 'zunzun/flyutil/fs.mjs'

export default class Init {

  static async input_vars(list) {
    try {
      let rl = CLIUtil.InputReader();
      const values = {};
      for (let ivar of list) {
        console.log(ivar.description);
        values[ivar.name] = await rl.questionSync(`${ivar.name}[${ivar.default}]: `);
        if (values[ivar.name] == "") values[ivar.name] = ivar.default;
      }
      rl.close();
      return values;
    } catch (e) {
      console.error(e.stack);
      return undefined;
    }
  }

  static async from_template(dst_path, tpl_cfg, input_vals, zunzun_cfg) {
    try {


      for (let rf of tpl_cfg.render_files) {
        const rfile_path = path.resolve(dst_path, rf);
        let rfile = fs.readFileSync(rfile_path, "utf8");
        for (let vname in input_vals) {
          rfile = rfile.replace(new RegExp(`<%( *)${vname}( *)%>`), input_vals[vname]);
        }
        fs.writeFileSync(rfile_path, rfile);
      }
    } catch (e) {
      console.error(e.stack);
    }
  }

  static async by_command(cwd, args, zunzun_cfg) {
    try {
      console.log(args);
      if (args.length > 0) {
        if (args[0] === "--tpl") {

        } else {
          const project_path = path.resolve(cwd, args[0])

          if (!fs.existsSync(project_path)) {

          } else {

          }

        }
      } else {
        const tpl_path = path.resolve(zunzun_cfg.zunzun_path, "tpl/app");
        const tpl_cfg_path = path.resolve(tpl_path, ".tplconfig.yaml");
        const tpl_cfg = YAML.parse(fs.readFileSync(tpl_cfg_path, "utf8"));
        const input_vals = await Init.input_vars(tpl_cfg.input_vars);

        const dst_path = path.resolve(cwd, input_vals["app_name"] || "new_app");
        
        FSUtil.copy_dir(tpl_path, dst_path);
        FSUtil.rm(path.resolve(dst_path, ".tplconfig.yaml"));
        await Init.from_template(dst_path, tpl_cfg, input_vals, zunzun_cfg);
      }
      return;
    } catch (e) {
      console.error(e.stack);
      return undefined;
    }
  }
}
