
import Install from "./src/install.mjs";
import Init from "./src/init.mjs";
import Publish from "./src/publish.mjs";
import LocalRepos from "./src/local-repos.mjs";

export default class Twigs {
  static Install = Install
  static Init = Init
  static Publish = Publish
  static LocalRepos = LocalRepos
}
